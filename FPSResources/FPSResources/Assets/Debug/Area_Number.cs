﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[ExecuteInEditMode]
public class Area_Number : MonoBehaviour
{
    Vector3 center;
    void Awake()
    {
        center = calCenter();
    }
#if UNITY_EDITOR
    void OnDrawGizmos()
    {
        Handles.color = Color.red;
        Handles.Label(center, this.transform.name, new GUIStyle() { fontSize = 40 }) ;
    }
#endif

    Vector3 calCenter()
    {
        Vector3 v = Vector3.zero;
        Transform[] g = this.GetComponentsInChildren<Transform>();
        if (g.Length > 0)
        {
            foreach (var i in g)
            {
                v += i.position;
            }
            v = v / g.Length;
            return v;
        }
        return Vector3.zero;
    }
}
