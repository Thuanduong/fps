﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public static class MoveToCenter
{
    [MenuItem("GameObject/Create Center Selected %h")]
    private static void GroupSelected()
    {
        if (!Selection.activeTransform) return;
        var go = new GameObject("Center");
        //go.transform.SetParent(Selection.activeTransform.parent, false);
        Vector3 center = calCenter(Selection.transforms);
        go.transform.position = center;
        Selection.activeGameObject = go;
    }

    static Vector3 calCenter(Transform[] g)
    {
        
        if (g != null && g.Length > 0)
        {
            Vector3 v = Vector3.zero;Debug.Log(g.Length);
            foreach (var i in g)
            {
                v += i.position;
            }
            v = v / g.Length;
            return v;
        }
        return Vector3.zero;
    }
}
