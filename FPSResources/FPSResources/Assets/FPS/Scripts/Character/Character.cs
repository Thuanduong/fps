﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour
{
    [SerializeField] string m_identify;
    [SerializeField] GameDefine.CHARACTER_TYPE m_type;
    [SerializeField] BulletController gun;
    [SerializeField] Transform m_weapon_holder;
    #region Var
    Animator m_animator;
    #endregion

    #region Public Atribute
    public string Identify
    {
        get { return m_identify; }
    }

    public GameDefine.CHARACTER_TYPE Type
    {
        get { return m_type; }
    }

    public int HP { get; private set; } = 0;

    public Vector3 position
    {
        get { return this.transform.position; }
    }

    public Quaternion rotation
    {
        get { return this.transform.rotation; }
    }

    public Vector3 direction
    {
        get { return this.transform.forward; }
    }

    public bool isActive
    {
        get { return this.gameObject.activeSelf; }
    }

    public bool canDead
    {
        get { return HP == 0; }
    }

    public bool isDead { get; private set; }

    #endregion

    #region Set Atribute
    public virtual void Setup()
    {
        this.m_animator = this.GetComponent<Animator>();
    }

    public Character SetIdentify(string _id)
    {
        this.m_identify = _id;
        return this;
    }

    public Character SetType(GameDefine.CHARACTER_TYPE _type)
    {
        this.m_type = _type;
        return this;
    }

    public Character SetHP (int value)
    {
        this.HP = value;
        if (HP < 0) HP = 0;
        if (HP > 0) isDead = false;
        return this;
    }

    public virtual Character SetPosition(Vector3 position)
    {
        this.transform.position = position;
        return this;
    }

    public virtual Character SetRotation(Quaternion quaternion)
    {
        this.transform.rotation = quaternion;
        return this;
    }

    public Character SetActive(bool active)
    {
        this.gameObject.SetActive(active);
        return this;
    }

    public Character SetWeapon(GameObject weapon)
    {
        if(m_weapon_holder != null)
        {
            weapon.transform.SetParent(m_weapon_holder);
            weapon.transform.localPosition = Vector3.zero;
            weapon.transform.rotation = Quaternion.identity;
        }
        return this;
    }

    public Character SetGunCallback(System.Action<GameObject> onHit)
    {
        if (gun != null) gun.RegisterListener(onHit);
        return this;
    }
    #endregion

    #region Animation

    public Character SetSpeed(float f)
    {
        this.m_animator.SetFloat("Speed_f", f);
        return this;
    }

    public Character Jump(bool value)
    {
        this.m_animator.SetBool("Jump_b", value);
        return this;
    }
      
    public Character Shot(bool value)
    {
        this.m_animator.SetBool("Shoot_b", value);
        return this;
    }

    public Character Shot(bool value, Vector3 direction)
    {
        this.m_animator.SetBool("Shoot_b", value);
        this.gun.Shot(direction);
        return this;
    }

    public Character SetWeapon(GameDefine.WEAPON_TYPE type)
    {
        return this.SetWeapon((int)type);
    }

    public Character SetWeapon(int type)
    {
        this.m_animator.SetInteger("WeaponType_int", type);
        return this;
    }

    public Character SetAnimation(GameDefine.ANIMATION_TYPE type)
    {
        return this.SetAnimation((int)type);
    }

    public Character SetAnimation(int type)
    {
        this.m_animator.SetInteger("Animation_int", type);
        return this;
    }

    public Character SetBodyVertical (float value)
    {
        this.m_animator.SetFloat("Body_Vertical_f", value);
        return this;
    }

    public Character SetDead(bool value)
    {
        isDead = value;
        this.m_animator.SetBool("Death_b", value);
        return this;
    }

    #endregion



}
