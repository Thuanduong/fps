﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : Character
{
    [SerializeField] NavMeshAgent m_agent;
    [SerializeField] GameDefine.ENEMY_LEVEL m_level;

    public override void Setup()
    {
        base.Setup();
        m_agent = GetComponent<NavMeshAgent>();
        this.SetType(GameDefine.CHARACTER_TYPE.ENEMY);
    }

    #region Var
    public GameDefine.ENEMY_LEVEL Level
    {
        get { return m_level; }
    }
    #endregion

    #region Set Atribute

    public Enemy SetAgent(NavMeshAgent _agent)
    {
        this.m_agent = _agent;
        return this;
    }

    public Enemy SetAgentActive(bool value)
    {
        this.m_agent.enabled = value;
        return this;
    }

    public Enemy SetLevel(GameDefine.ENEMY_LEVEL level)
    {
        this.m_level = level;
        return this;
    }

    public Enemy SetMoveTarget(Vector3 target, float Speed = 300f)
    {
        this.m_agent.speed = Speed;
        this.m_agent.SetDestination(target);
        return this;
    }

    public Enemy SetAgentPosition(Vector3 position)
    {
        this.m_agent.Warp(position);
        return this;
    }

    #endregion

}
