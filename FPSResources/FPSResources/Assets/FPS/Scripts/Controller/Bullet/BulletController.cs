﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MonoBehaviour
{
    BulletObject[] bullets = new BulletObject[30];
    List<GameObject> bulletObjPools = new List<GameObject>();

    Queue<BulletObject> NotUsed = new Queue<BulletObject>();
    Queue<GameObject> NotUsedObj = new Queue<GameObject>();
    Dictionary<BulletObject, GameObject> activeBullet = new Dictionary<BulletObject, GameObject>();

    GameObject hitObj;

    event System.Action<GameObject> OnHitCallBack;

    [SerializeField] LayerMask gunLayerMash;
    [SerializeField] ParticleSystem shotEffect;
    [SerializeField] GameObject bulletPrefab;
    [SerializeField] float bulletSpeed = 100f;

    private GameObject shotHolder;

    // Start is called before the first frame update
    void Start()
    {
        InitBullets();
    }

    // Update is called once per frame
    void Update()
    {
        UpdateBullets();
    }

    void InitBullets()
    {
        for(int i = 0; i < bullets.Length; i++)
        {
            bullets[i] = new BulletObject();
            NotUsed.Enqueue(bullets[i]);
        }
    }

    public BulletController SetShotHolder(GameObject _holder)
    {
        this.shotHolder = _holder;
        return this;
    }

    public void RegisterListener(System.Action<GameObject> onHit)
    {
        OnHitCallBack += onHit;
    }

    public void UnRegisterListener(System.Action<GameObject> onHit)
    {
        OnHitCallBack -= onHit;
    }

    public bool Shot(Vector3 dir)
    {
        return Shot(dir, bulletSpeed);
    }

    public bool Shot(Vector3 dir, float speed)
    {
        if(NotUsed.Count > 0)
        {
            BulletObject b = NotUsed.Dequeue();
            GameObject bs = getBullet();
            bs.SetActive(true);
            b.Shot(this.transform.position, dir, speed);
            bs.transform.position = b.position;
            bs.transform.forward = dir;
            shotEffect.Play();
            activeBullet.Add(b, bs);
            return true;
        }
        return false;
    }

    void UpdateBullets()
    {
        if (NotUsed.Count == bullets.Length) return;

        for(int i = 0; i < bullets.Length; i++)
        {
            if(bullets[i].speed > 0)
            {
                bullets[i].Update();
                if (activeBullet.ContainsKey(bullets[i])) {
                    if (!activeBullet[bullets[i]].activeSelf)
                        activeBullet[bullets[i]].SetActive(true);
                    activeBullet[bullets[i]].transform.position = bullets[i].position;
                }
                if(bullets[i].IsHit(out hitObj))
                {
                    if (hitObj != null)
                    {
                        if ((1 << hitObj.layer | gunLayerMash) == gunLayerMash) return;
                        Debug.Log(hitObj.name);
                        if (OnHitCallBack != null) OnHitCallBack.Invoke(hitObj);
                    }
                    bullets[i].Stop();
                    NotUsed.Enqueue(bullets[i]);
                    activeBullet[bullets[i]].SetActive(false);
                    NotUsedObj.Enqueue(activeBullet[bullets[i]]);
                    activeBullet.Remove(bullets[i]);
                    
                }
            }
        }
    }
    
    GameObject getBullet()
    {
        if(NotUsedObj.Count > 0)
        {
            return NotUsedObj.Dequeue();
        }

        if(bulletObjPools.Count < 30)
        {
            GameObject b = Instantiate(bulletPrefab);
            b.SetActive(false);
            bulletObjPools.Add(b);
            return b;
        }
        return null;
    }
}
