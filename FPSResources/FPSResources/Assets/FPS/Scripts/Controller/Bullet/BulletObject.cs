﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletObject 
{
    public float speed { get; private set; } = 0;

    public Vector3 position { get; private set; }
    public Vector3 direction { get; private set; }
    private Vector3 lastPosition;

    private float length;
    RaycastHit m_Hit;
    bool m_HitDetect;

    Bounds bound = new Bounds();

    public BulletObject SetPosition(Vector3 _position)
    {
        this.position = _position;
        this.lastPosition = _position;
        return this;
    }

    public bool IsHit(out GameObject obj)
    {
        obj = null;
        m_HitDetect = Physics.Linecast(position, lastPosition, out m_Hit);
        if (m_HitDetect)
        {
            obj = m_Hit.transform.gameObject;
        }
        if (length > 1000f)
        {
            m_HitDetect = true;
        }
        return m_HitDetect;
    }

    public BulletObject Shot(Vector3 _position, Vector3 _direction, float _speed)
    {
        length = 0;
        this.direction = _direction;
        this.speed = _speed;
        SetPosition(_position);
        return this;
    }

    public void Stop()
    {
        speed = 0;
    }

    public void Update()
    {
        if (speed <= 0) return;
        lastPosition = position;
        position += speed * direction * Time.deltaTime;
        length += Vector3.Distance(position, lastPosition);
    }
}
