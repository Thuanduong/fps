﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField] private Vector2 sensitivity = Vector2.zero;
    [SerializeField] private Vector2 smoothAmount = Vector2.zero;
    [SerializeField] [Range(-90f, 90f)] private float lookAngleMinMaxX = 0;
    [SerializeField] [Range(-90f, 90f)] private float lookAngleMinMaxY = 0;

    #region Var
    private float m_yaw;
    private float m_pitch;

    private float m_desiredYaw;
    private float m_desiredPitch;

    #region Components                    
    private Transform m_pitchTranform;
    private Camera m_cam;
    #endregion

    #endregion

    public Vector3 Direction
    {
        get
        {
            if (m_cam == null) return Vector3.forward;
            return m_cam.transform.forward;
        }
    }

    public float VerticalPercent
    {
        get
        {
            if (m_desiredPitch >= 0) return lookAngleMinMaxY != 0 ? -(m_desiredPitch / lookAngleMinMaxY) : 0;
            else return lookAngleMinMaxX != 0 ? (m_desiredPitch / lookAngleMinMaxX) : 0;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        GetComponents();
        InitValues();
        //ChangeCursorState();
    }

    // Update is called once per frame
    void LateUpdate()
    {
        CalculateRotation();
        SmoothRotation();
        ApplyRotation();
    }

    #region Custom Methods
    void GetComponents()
    {
        m_pitchTranform = transform.GetChild(0).transform;
        m_cam = GetComponentInChildren<Camera>();
    }

    void InitValues()
    {
        m_yaw = transform.eulerAngles.y;
        m_desiredYaw = m_yaw;
    }

    void CalculateRotation()
    {
        m_desiredYaw += InputController.cameraInputData.InputVector.x * sensitivity.x * Time.deltaTime;
        m_desiredPitch -= InputController.cameraInputData.InputVector.y * sensitivity.y * Time.deltaTime;

        m_desiredPitch = Mathf.Clamp(m_desiredPitch, lookAngleMinMaxX, lookAngleMinMaxY);
    }

    void SmoothRotation()
    {
        m_yaw = Mathf.Lerp(m_yaw, m_desiredYaw, smoothAmount.x * Time.deltaTime);
        m_pitch = Mathf.Lerp(m_pitch, m_desiredPitch, smoothAmount.y * Time.deltaTime);
    }

    void ApplyRotation()
    {
        transform.eulerAngles = new Vector3(0f, m_yaw, 0f);
        m_pitchTranform.localEulerAngles = new Vector3(m_pitch, 0f, 0f);
    }
    
    void ChangeCursorState()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }
    #endregion
}
