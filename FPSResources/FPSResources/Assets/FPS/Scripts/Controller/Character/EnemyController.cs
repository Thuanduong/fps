﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : Singleton<EnemyController>
{
    public float MaxSpeedMove = 1f;
    public float MinSpeedMove = 0.5f;
    public float MaxAgentMove = 5f;
    public float MinAgentMove = 1f;

    [SerializeField] GameObject[] EnemyPrefab;

    #region var
    private PoolSystem m_pool;

#if UNITY_EDITOR
    public List<Enemy> m_enemyList = new List<Enemy>();
#else
    private List<Enemy> m_enemyList = new List<Enemy>();
#endif
    public Dictionary<string, Enemy> m_enemyDict = new Dictionary<string, Enemy>();

    Queue<Enemy> ReadyUsed = new Queue<Enemy>();
    Queue<Enemy> NotUsed = new Queue<Enemy>();

    int m_currentUpdateIndex = 0;

    public Player player { get; private set; }

    [SerializeField] float m_playerRange;
    [SerializeField] float m_destroyRange;

    private Vector3 p_pos, p_direction;

    private bool hide = false;
    private bool chase = true;
    private bool attack = false;
    private float speed = 0;

    #endregion

    void Awake()
    {
        Init(this);
    }
    
    void Update()
    {
        Tick();
    }

    #region Initizalize

    public IEnumerator CreateEnemies(int num)
    {
        for(int i = 0; i < num; i++)
        {
            int r = Random.Range(0, 2);
            Enemy e =  CreateEnemy(r == 1 ? GameDefine.ENEMY_LEVEL.LVL_2 : GameDefine.ENEMY_LEVEL.LVL_1);
            if (e != null)
            {
                m_enemyList.Add(e);
                if(!m_enemyDict.ContainsKey(e.Identify)) m_enemyDict.Add(e.Identify, e);
                ReadyUsed.Enqueue(e);
                e.SetActive(false);
            }
            yield return null;
        }
    }

    public IEnumerator ReCreateEnemies()
    {
        yield return KillEnemyNotInRange();
        int count = NotUsed.Count;
        yield return CreateEnemies(count);
    }

    public Enemy CreateEnemy(GameDefine.ENEMY_LEVEL level = GameDefine.ENEMY_LEVEL.LVL_1)
    {
        if(NotUsed.Count > 0)
        {
            Enemy e = NotUsed.Dequeue();
            e.SetAgentActive(true)
                .SetLevel(level)
                .SetActive(true)
                .SetHP(100)
                .SetDead(false);
            return e;
        }
        else if(m_enemyDict.Count < 20)
        {
            int rand = EnemyPrefab.Length;
            rand = Random.Range(0, rand);
            GameObject o = Instantiate(EnemyPrefab[rand]);
            Enemy e = o.GetComponent<Enemy>();
            e.name = e.GetInstanceID().ToString();
            e.Setup();
            e.SetAgentActive(true)
                .SetLevel(level)
                .SetActive(true)
                .SetHP(100)
                .SetDead(false)
                .SetIdentify(e.name);
            return e;
        }
        return null;
    }

    #endregion

    #region Spawn

    public IEnumerator SpawnEnemy(Bounds bound)
    {
        while(ReadyUsed.Count > 0)
        {
            Enemy e = ReadyUsed.Dequeue();
            Vector3 v = RandomPointInBox(bound.center, bound.size);
            v.y = 0;
            e.SetAgentPosition(v).SetActive(true);
            yield return null;
        }
    }

    IEnumerator KillEnemyNotInRange()
    {
        for(int i = m_enemyList.Count - 1; i >= 0; i--)
        {
            Enemy e = m_enemyList[i];
            Vector3 m_pos = e.position;
            float distance = Helper.Distance(m_pos, p_pos);
            if (distance > m_playerRange)
            {
                e.SetDead(true);
                e.SetAgentActive(false);
                e.gameObject.SetActive(false);
                m_enemyList.Remove(e);
                NotUsed.Enqueue(e);
            }
            yield return null;
        }
    }

    #endregion

    #region Update AI

    public void Tick()
    {
        if (m_enemyList.Count == 0) return;
        if (player == null) return;
        p_pos = player.position;
        p_direction = player.direction;

        if (m_currentUpdateIndex >= m_enemyList.Count)
            m_currentUpdateIndex = 0;
        Enemy enemy = m_enemyList[m_currentUpdateIndex];
        UpdateAI(enemy);
        m_currentUpdateIndex++;
        
    }

    void UpdateAI(Enemy enemy)
    {
        //Check Position With Player
        Vector3 m_pos = enemy.position;
        Vector3 m_direction = enemy.direction;
        float distance = Helper.Distance(m_pos, p_pos);
        hide = false;
        chase = true;
        if (enemy.isDead) return;

        if (distance > m_destroyRange)
        {
            KillEnemy(enemy);
            chase = false;
            return;
        }
        else if (distance > m_playerRange)
        {
            //enemy.SetActive(false);
            //hide = true;
            chase = false;
        }
        else if (!enemy.isActive)
        {
            enemy.SetActive(true);
        }

        //Update Position
        if(player.isDead)
        {
            chase = false;
            attack = true;
        }
        if (chase)
        {
            enemy.SetAgentActive(true);
            enemy.SetMoveTarget(p_pos, getAgentSpeed(enemy));
        }

        //Update Anim
        if (hide) return;

        if (attack)
            enemy.Shot(attack);
        else if (enemy.canDead && !enemy.isDead)
            enemy.SetDead(true);
        else if (chase)
        {
            enemy.SetSpeed(getMaxSpeed(enemy));
        }
    }

    void DestroyEnemy()
    {

    }
    
    #endregion

    #region Check

    bool IsInRange(Vector3 p1, Vector3 p2)
    {
        return Helper.Distance(p1, p2) < m_playerRange;
    }

    float getMaxSpeed(Enemy enemy)
    {
        if (enemy.Level == GameDefine.ENEMY_LEVEL.LVL_2)
            return MaxSpeedMove;
        else
            return MinSpeedMove;
    }

    float getAgentSpeed(Enemy enemy)
    {
        if (enemy.Level == GameDefine.ENEMY_LEVEL.LVL_2)
            return MaxAgentMove;
        else
            return MinAgentMove;
    }


    #endregion

    public void OnHitEnemy(string name)
    {
        if (!m_enemyDict.ContainsKey(name)) return;

        Enemy e = m_enemyDict[name];
        e.SetHP(e.HP - 40);
        if (e.canDead) KillEnemy(e);
    }

    void KillEnemy(Enemy e )
    {
        e.SetDead(true);
        e.SetAgentActive(false);
        StartCoroutine(Hide(e));
    }

    IEnumerator Hide(Enemy e)
    {
        yield return new WaitForSeconds(2f);
        e.gameObject.SetActive(false);
        m_enemyList.Remove(e);
        NotUsed.Enqueue(e);
    }
    
    private static Vector3 RandomPointInBox(Vector3 center, Vector3 size)
    {

        return center + new Vector3(
           (Random.value - 0.5f) * size.x,
           0,
           (Random.value - 0.5f) * size.z
        );
    }

    public void SetPlayer(Player _p)
    {
        player = _p;
    }
}
