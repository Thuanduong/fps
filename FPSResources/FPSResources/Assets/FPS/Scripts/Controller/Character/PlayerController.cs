﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    #region Locomotion
    [Space, Header("Locomotion Settings")]
    [SerializeField] private float crouchSpeed = 1f;
    [SerializeField] private float walkSpeed = 2f;
    [SerializeField] private float runSpeed = 3f;
    [SerializeField] private float jumpSpeed = 5f;
    [Range(0f, 1f)] [SerializeField] private float moveBackwardsSpeedPercent = 0.5f;
    [Range(0f, 1f)] [SerializeField] private float moveSideSpeedPercent = 0.75f;
    #endregion

    #region Landing Settings
    [Space, Header("Landing Settings")]
    [Range(0.05f, 0.5f)] [SerializeField] private float lowLandAmount = 0.1f;
    [Range(0.2f, 0.9f)] [SerializeField] private float highLandAmount = 0.6f;
    [SerializeField] private float landTimer = 0.5f;
    [SerializeField] private float landDuration = 1f;
    [SerializeField] private AnimationCurve landCurve = AnimationCurve.EaseInOut(0f, 0f, 1f, 1f);
    #endregion

    #region Shoting Settings
    [Space, Header("Shoting Settings")]
    [SerializeField] private float shotTimer = 0.1f;
    [SerializeField] private LayerMask enemyLayer;
    #endregion

    #region Gravity
    [Space, Header("Gravity Settings")]
    [SerializeField] private float gravityMultiplier = 2.5f;
    [SerializeField] private float stickToGroundForce = 5f;

    [SerializeField] private LayerMask groundLayer = ~0;
    [Range(0f, 1f)] [SerializeField] private float rayLength = 0.1f;
    [Range(0.01f, 1f)] [SerializeField] private float raySphereRadius = 0.1f;
    #endregion

    #region Run Settings
    [Space, Header("Run Settings")]
    [Range(-1f, 1f)] [SerializeField] private float canRunThreshold = 0.8f;
    [SerializeField] private AnimationCurve runTransitionCurve = AnimationCurve.EaseInOut(0f, 0f, 1f, 1f);
    #endregion

    #region Smooth Settings
    [Space, Header("Smooth Settings")]
    [Range(1f, 100f)] [SerializeField] private float smoothRotateSpeed = 5f;
    [Range(1f, 100f)] [SerializeField] private float smoothInputSpeed = 5f;
    [Range(1f, 100f)] [SerializeField] private float smoothVelocitySpeed = 5f;
    [Range(1f, 100f)] [SerializeField] private float smoothFinalDirectionSpeed = 5f;
    [Range(1f, 100f)] [SerializeField] private float smoothInputMagnitudeSpeed = 5f;
    #endregion

    #region Wall Settings
    [Space, Header("Check Wall Settings")]
    [SerializeField] private LayerMask obstacleLayers = ~0;
    [Range(0f, 1f)] [SerializeField] private float rayObstacleLength = 0.1f;
    [Range(0.01f, 1f)] [SerializeField] private float rayObstacleSphereRadius = 0.1f;

    #endregion

    #region Property

    public Vector3 position => this.transform.position;

    #endregion

    #region Var
    private CharacterController m_characterController;
    private Transform m_yawTransform;
    private Transform m_camTransform;
    private CameraController m_cameraController;
    public Player player { get; private set; }

    private RaycastHit m_hitInfo;
    private MovementInputData movementInputData;

    private Vector3 startPosition;
    #region Calculate
    private Vector2 m_inputVector;
    private Vector2 m_smoothInputVector;

    
    private Vector3 m_finalMoveDir;
    private Vector3 m_smoothFinalMoveDir;
    
    private Vector3 m_finalMoveVector;

    
    private float m_currentSpeed;
    private float m_smoothCurrentSpeed;
    private float m_finalSmoothCurrentSpeed;
    private float m_walkRunSpeedDifference;

    
    private float m_finalRayLength;
    private bool m_hitWall;
    private bool m_isGrounded;
    private bool m_previouslyGrounded;

    private bool m_canShot;
    private bool m_isShoted;
    private bool m_previouslyShoted;

    private bool m_canHurt;
    private bool m_isHurted;
    private bool m_previouslyHurted;


    private float m_initHeight;
    private float m_crouchHeight;
    private Vector3 m_initCenter;
    private Vector3 m_crouchCenter;
    
    private float m_initCamHeight;
    private float m_crouchCamHeight;
    private float m_crouchStandHeightDifference;
    private bool m_duringCrouchAnimation;
    private bool m_duringRunAnimation;
    
    private float m_inAirTimer;

    private float m_inputVectorMagnitude;
    private float m_smoothInputVectorMagnitude;

    private IEnumerator m_LandRoutine;
    private IEnumerator m_ShotRoutine;
    private IEnumerator m_HurtRoutine;

    #endregion

    #endregion

    // Start is called before the first frame update
    void Start()
    {
        GetComponents();
        InitVariables();
    }

    // Update is called once per frame
    void Update()
    {
        if (m_yawTransform != null)
            RotateTowardsCamera();
        
        if (m_characterController)
        {
            if (player.canDead && !player.isDead)
            {
                player.SetDead(true);
            }

            if (player.isDead) return;

            // Check if Grounded,Wall etc
            CheckIfGrounded();
            CheckIfWall();

            // Apply Smoothing
            SmoothInput();
            SmoothSpeed();
            SmoothDir();
            
            // Calculate Movement
            CalculateMovementDirection();
            CalculateSpeed();
            CalculateFinalMovement();

            // Handle Player Movement, Gravity, Jump, Crouch etc.
            HandleLanding();
            HandleShot();
            HandleHurt();

            ApplyGravity();
            ApplyMovement();

            m_previouslyGrounded = m_isGrounded;
            m_previouslyShoted = m_isShoted;
            m_previouslyHurted = m_isHurted;
        }
    }

    #region Methods

    #region Initialize Methods    
    protected virtual void GetComponents()
    {
        m_characterController = GetComponent<CharacterController>();
        m_cameraController = GetComponentInChildren<CameraController>();
        m_yawTransform = m_cameraController.transform;
        m_camTransform = GetComponentInChildren<Camera>().transform;
        player = GetComponent<Player>();
    }

    protected virtual void InitVariables()
    {
        // Calculate where our character center should be based on height and skin width
        m_characterController.center = new Vector3(0f, m_characterController.height / 2f + m_characterController.skinWidth, 0f);
        player.Setup();
        player.SetHP(100).SetWeapon(GameDefine.WEAPON_TYPE.AssultRifle01).SetGunCallback(OnHitCallBack);
        movementInputData = InputController.movementInputData;

        m_initCenter = m_characterController.center;
        m_initHeight = m_characterController.height;
        
        m_initCamHeight = m_yawTransform.localPosition.y;

        m_finalRayLength = rayLength + m_characterController.center.y;

        m_isGrounded = true;
        m_previouslyGrounded = true;

        m_isShoted = false;
        m_previouslyShoted = false;
        m_canShot = true;

        m_canHurt = true;
        m_isHurted = false;
        m_previouslyHurted = false;

        m_inAirTimer = 0f;
        m_walkRunSpeedDifference = runSpeed - walkSpeed;

        SetPosition(startPosition);
    }
    #endregion

    #region Smoothing Methods
    protected virtual void SmoothInput()
    {
        m_inputVector = movementInputData.InputVector.normalized;
        m_smoothInputVector = Vector2.Lerp(m_smoothInputVector, m_inputVector, Time.deltaTime * smoothInputSpeed);
    }

    protected virtual void SmoothSpeed()
    {
        m_smoothCurrentSpeed = Mathf.Lerp(m_smoothCurrentSpeed, m_currentSpeed, Time.deltaTime * smoothVelocitySpeed);

        float _walkRunPercent = Mathf.InverseLerp(walkSpeed, runSpeed, m_smoothCurrentSpeed);
        m_finalSmoothCurrentSpeed = runTransitionCurve.Evaluate(_walkRunPercent) * m_walkRunSpeedDifference + walkSpeed;
    }

    protected virtual void SmoothDir()
    {

        m_smoothFinalMoveDir = Vector3.Lerp(m_smoothFinalMoveDir, m_finalMoveDir, Time.deltaTime * smoothFinalDirectionSpeed);
        Debug.DrawRay(transform.position, m_smoothFinalMoveDir, Color.yellow);
    }

    protected virtual void SmoothInputMagnitude()
    {
        m_inputVectorMagnitude = m_inputVector.magnitude;
        m_smoothInputVectorMagnitude = Mathf.Lerp(m_smoothInputVectorMagnitude, m_inputVectorMagnitude, Time.deltaTime * smoothInputMagnitudeSpeed);
    }
    #endregion

    #region Locomotion Calculation Methods
    protected virtual void CheckIfGrounded()
    {
        Vector3 _origin = transform.position + m_characterController.center;

        bool _hitGround = Physics.SphereCast(_origin, raySphereRadius, Vector3.down, out m_hitInfo, m_finalRayLength, groundLayer);
        Debug.DrawRay(_origin, Vector3.down * (m_finalRayLength), Color.red);

        m_isGrounded = _hitGround ? true : false;
    }

    protected virtual void CheckIfWall()
    {

        Vector3 _origin = transform.position + m_characterController.center;
        RaycastHit _wallInfo;

        bool _hitWall = false;

        if (movementInputData.HasInput && m_finalMoveDir.sqrMagnitude > 0)
            _hitWall = Physics.SphereCast(_origin, rayObstacleSphereRadius, m_finalMoveDir, out _wallInfo, rayObstacleLength, obstacleLayers);
        Debug.DrawRay(_origin, m_finalMoveDir * rayObstacleLength, Color.blue);

        m_hitWall = _hitWall ? true : false;
    }

    protected virtual bool CheckIfRoof() /// TO FIX
    {
        Vector3 _origin = transform.position;
        RaycastHit _roofInfo;

        bool _hitRoof = Physics.SphereCast(_origin, raySphereRadius, Vector3.up, out _roofInfo, m_initHeight);

        return _hitRoof;
    }

    protected virtual bool CanRun()
    {
        Vector3 _normalizedDir = Vector3.zero;

        if (m_smoothFinalMoveDir != Vector3.zero)
            _normalizedDir = m_smoothFinalMoveDir.normalized;

        float _dot = Vector3.Dot(transform.forward, _normalizedDir);
        return _dot >= canRunThreshold ? true : false;
    }

    protected virtual void CalculateMovementDirection()
    {

        Vector3 _vDir = transform.forward * m_smoothInputVector.y;
        Vector3 _hDir = transform.right * m_smoothInputVector.x;

        Vector3 _desiredDir = _vDir + _hDir;
        Vector3 _flattenDir = FlattenVectorOnSlopes(_desiredDir);

        m_finalMoveDir = _flattenDir;
    }

    protected virtual Vector3 FlattenVectorOnSlopes(Vector3 _vectorToFlat)
    {
        if (m_isGrounded)
            _vectorToFlat = Vector3.ProjectOnPlane(_vectorToFlat, m_hitInfo.normal);

        return _vectorToFlat;
    }

    protected virtual void CalculateSpeed()
    {
        m_currentSpeed = runSpeed;
        m_currentSpeed = !movementInputData.HasInput ? 0f : m_currentSpeed;
        m_currentSpeed = movementInputData.InputVector.y == -1 ? m_currentSpeed * moveBackwardsSpeedPercent : m_currentSpeed;
        m_currentSpeed = movementInputData.InputVector.x != 0 && movementInputData.InputVector.y == 0 ? m_currentSpeed * moveSideSpeedPercent : m_currentSpeed;
    }

    protected virtual void CalculateFinalMovement()
    {
        float _smoothInputVectorMagnitude = 1f;
        Vector3 _finalVector = m_smoothFinalMoveDir * m_finalSmoothCurrentSpeed * _smoothInputVectorMagnitude;

        m_finalMoveVector.x = _finalVector.x;
        m_finalMoveVector.z = _finalVector.z;

        if (m_characterController.isGrounded) 
            m_finalMoveVector.y += _finalVector.y; 
    }
    #endregion

    #region Landing Methods
    protected virtual void HandleLanding()
    {
        if (!m_previouslyGrounded && m_isGrounded)
        {
            player.Jump(false);
            InvokeLandingRoutine();
        }
    }

    protected virtual void InvokeLandingRoutine()
    {
        if (m_LandRoutine != null)
            StopCoroutine(m_LandRoutine);

        m_LandRoutine = LandingRoutine();
        StartCoroutine(m_LandRoutine);
    }

    protected virtual IEnumerator LandingRoutine()
    {
        float _percent = 0f;
        float _landAmount = 0f;

        float _speed = 1f / landDuration;

        Vector3 _localPos = m_yawTransform.localPosition;
        float _initLandHeight = _localPos.y;

        _landAmount = m_inAirTimer > landTimer ? highLandAmount : lowLandAmount;

        while (_percent < 1f)
        {
            _percent += Time.deltaTime * _speed;
            float _desiredY = landCurve.Evaluate(_percent) * _landAmount;

            _localPos.y = _initLandHeight + _desiredY;
            m_yawTransform.localPosition = _localPos;

            yield return null;
        }
    }
    #endregion

    #region Shot Methods
    protected virtual void HandleShot()
    {
        if(m_isShoted && !m_canShot )
        {
            m_isShoted = false;
            InvokeShotingRoutine();
        }

        if (movementInputData.ShotClicked && !m_isShoted && m_canShot && m_isGrounded)
        {
            player.Shot(true, m_cameraController.Direction);
            m_previouslyShoted = false;
            m_isShoted = true;
            m_canShot = false;
        }
    }

    protected virtual void InvokeShotingRoutine()
    {
        if (m_ShotRoutine != null) return;

        m_ShotRoutine = ShotingRoutine();
        StartCoroutine(m_ShotRoutine);
    }

    protected virtual IEnumerator ShotingRoutine()
    {
        yield return new WaitForSeconds(shotTimer);
        m_canShot = true;
        player.Shot(false);
        m_ShotRoutine = null;
    }

    #endregion

    #region Hurt Methods
    protected virtual void HandleHurt()
    {

        if (m_isHurted && !m_canHurt)
        {
            m_isHurted = false;
        }

        if (m_isHurted && m_canHurt)
        {
            m_isHurted = true;
            m_canHurt = false;
            OnHurt();
            InvokeHurtRoutine();
        }


    }

    protected virtual void InvokeHurtRoutine()
    {
        if (m_HurtRoutine != null) return;

        m_HurtRoutine = HurtRoutine();
        StartCoroutine(m_HurtRoutine);
    }

    protected virtual IEnumerator HurtRoutine()
    {
        yield return new WaitForSeconds(shotTimer);
        m_canHurt = true;
        m_HurtRoutine = null;
    }
    #endregion

    #region Locomotion Apply Methods

    protected virtual void HandleJump()
    {
        if (movementInputData.JumpClicked)
        {
            m_finalMoveVector.y = jumpSpeed;

            m_previouslyGrounded = true;
            m_isGrounded = false;
            player.Jump(true);
        }
    }
    protected virtual void ApplyGravity()
    {
        if (m_characterController.isGrounded) // if we would use our own m_isGrounded it would not work that good, this one is more precise
        {
            m_inAirTimer = 0f;
            m_finalMoveVector.y = -stickToGroundForce;
            
            HandleJump();
        }
        else
        {
            m_inAirTimer += Time.deltaTime;
            m_finalMoveVector += Physics.gravity * gravityMultiplier * Time.deltaTime;
        }
    }

    protected virtual void ApplyMovement()
    {
        m_characterController.Move(m_finalMoveVector * Time.deltaTime);
        player.SetSpeed(m_currentSpeed/runSpeed);
    }

    protected virtual void RotateTowardsCamera()
    {
        Quaternion _currentRot = transform.rotation;
        Quaternion _desiredRot = m_yawTransform.rotation;
        
        transform.rotation = Quaternion.Slerp(_currentRot, _desiredRot, Time.deltaTime * smoothRotateSpeed);

        player.SetBodyVertical(m_cameraController.VerticalPercent);
    }
    #endregion

    public void SetPosition(Vector3 position)
    {
        if(this.m_characterController == null)
        {
            startPosition = position;
            return;
        }
        this.m_characterController.enabled = false;
        this.transform.position = position;
        this.m_characterController.enabled = true;
    }

    public void OnHitCallBack(GameObject obj)
    {
        if (obj == null) return;
        if((1 << obj.layer | enemyLayer) == enemyLayer)
        {
            EnemyController.Instance.OnHitEnemy(obj.name);
            InGameController.OnPlayerHit();
        }
    }

    public void OnHurt()
    {
        player.SetHP(player.HP - 1);
        InGameController.OnPlayerHurt();
    }

    #endregion

    void OnTriggerEnter(Collider other)
    {
        if ((1 << other.gameObject.layer | enemyLayer) == enemyLayer)
        {
            m_isHurted = true;
        }
    }

    void OnTriggerStay(Collider other)
    {
        if ((1 << other.gameObject.layer | enemyLayer) == enemyLayer)
        {
            m_isHurted = true;
        }
    }
}
