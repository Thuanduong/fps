﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class InGameController : MonoBehaviour
{
    [SerializeField] string InitArea;
    [SerializeField] Vector3 InitPosition;
    [SerializeField] GameObject PlayerPrefab;

    public static InGameController instance;

    IEnumerator enemySpaw, endGame;

    #region Var
    PlayerController player;

    private bool isReady = false;
    
    public int Score { get; private set; }

    private int count_to_Ten;
    #endregion
    // Start is called before the first frame update
    void Start()
    {
        instance = this;
        StartCoroutine(Initgame());
    }
    // Update is called once per frame
    void Update()
    {
        if (!isReady) return;

        if (count_to_Ten >= 10)
        {
            SceneController.Instance.Tick(player.position);
            count_to_Ten = 0;
        }
        count_to_Ten++;

        HandleDead();
        
    }

    #region Methods
    #region Initialize
    IEnumerator Initgame()
    {
        isReady = false;
        Score = 0;
        Application.backgroundLoadingPriority = ThreadPriority.High;
        yield return StartLoadScene();

        yield return StartLoadCharacter();

        EnemyController.Instance.SetPlayer(player.player);
        yield return StartLoadEnemy();

        Application.backgroundLoadingPriority = ThreadPriority.Low;
        yield return new WaitForSeconds(2f);
        isReady = true;
        UIManager.ShowBlock(false);

        UpdateScore();
        UpdateHP();
    }

    IEnumerator StartLoadScene()
    {
        yield return DoLoadScene("s_game");

        yield return DoLoadScene("s_scene");

        yield return new WaitForEndOfFrame();

        bool wait = true;
        while (!SceneController.Instance.IsInit)
            yield return null;
        SceneController.Instance.LoadScenes(InitArea, () =>
        {
            wait = false;
        });
        while (wait)
            yield return null;
    }

    IEnumerator StartLoadCharacter()
    {
        GameObject go = Instantiate(PlayerPrefab);
        go.name = "MainPlayer";
        player = go.GetComponent<PlayerController>();
        player.SetPosition(InitPosition);
        yield return null;
    }

    IEnumerator StartLoadEnemy()
    {
        yield return EnemyController.Instance.CreateEnemies(20);

        while (!SceneController.Instance.IsAvaiBound)
            yield return null;

        yield return EnemyController.Instance.SpawnEnemy(SceneController.Instance.getCurrentBounds());
    }
    
    IEnumerator DoLoadScene(string scene, bool isSingle = false)
    {
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(scene, isSingle ? LoadSceneMode.Single : LoadSceneMode.Additive);
        
        while (!asyncLoad.isDone)
        {
            yield return null;
        }
    }
    
    #endregion

    void HandleDead()
    {
        if (player.player.isDead && endGame == null)
        {
            endGame = DoEndGame();
            StartCoroutine(endGame);
        }
    }

    IEnumerator DoEndGame()
    {
        yield return new WaitForSeconds(2f);
        UIManager.ShowBlock(true);
        yield return DoLoadScene("s_mainmenu", true);

    }
    #endregion

    #region GamePlay

    void UpdateScore()
    {
        UIManager.SetScore(Score);
    }

    void UpdateHP()
    {        
        UIManager.SetCurrentHP(player.player.HP);
    }

    IEnumerator ReLoadEnemy()
    {
        yield return EnemyController.Instance.ReCreateEnemies();

        while (!SceneController.Instance.IsAvaiBound)
            yield return null;

        yield return EnemyController.Instance.SpawnEnemy(SceneController.Instance.getCurrentBounds());

        yield return new WaitForSeconds(30f);
    }

    public static void OnPlayerHurt()
    {
        if (instance == null) return;
        instance.UpdateHP();
    }

    public static void OnPlayerHit()
    {
        if (instance == null) return;
        instance.Score += 10;
        instance.UpdateScore();
    }

    public static void OnEnterNewScene()
    {
        if (instance == null) return;
        if (instance.enemySpaw != null) return;
        instance.enemySpaw = instance.ReLoadEnemy();
        instance.StartCoroutine(instance.enemySpaw);
    }
    #endregion

}
