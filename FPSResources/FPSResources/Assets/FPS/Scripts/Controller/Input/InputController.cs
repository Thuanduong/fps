﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : MonoBehaviour
{
    [SerializeField] VariableJoystick moveJoyStick;
    [SerializeField] VariableJoystick rotateJoyStick;
    [SerializeField] SimpleButton shotBtn;
    [SerializeField] SimpleButton jumpBtn;

    public static InputData cameraInputData = new InputData();
    public static MovementInputData movementInputData = new MovementInputData();

    void Start()
    {
        cameraInputData.ResetInput();
        movementInputData.ResetInput();
    }

    void Update()
    {
        GetCameraInput();
        GetMovementInputData();
    }

    void GetCameraInput()
    {
        cameraInputData.InputVectorX = rotateJoyStick.Horizontal;
        cameraInputData.InputVectorY = rotateJoyStick.Vertical;
#if UNITY_EDITOR
        //if (!cameraInputData.HasInput)
        //{
        //    cameraInputData.InputVectorX = Input.GetAxis("Mouse X");
        //    cameraInputData.InputVectorY = Input.GetAxis("Mouse Y");
        //}
#endif
    }

    void GetMovementInputData()
    {
        movementInputData.InputVectorX = moveJoyStick.Horizontal;
        movementInputData.InputVectorY = moveJoyStick.Vertical;
#if UNITY_EDITOR
        if (!movementInputData.HasInput)
        {
            movementInputData.InputVectorX = Input.GetAxisRaw("Horizontal");
            movementInputData.InputVectorY = Input.GetAxisRaw("Vertical");
        }
#endif
        movementInputData.JumpClicked = jumpBtn.hasPress;
        movementInputData.ShotClicked = shotBtn.hasPress;

    }

}
