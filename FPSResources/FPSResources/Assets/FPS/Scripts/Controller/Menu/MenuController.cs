﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuController : MonoBehaviour
{
    [SerializeField] Button btnButton;
    [SerializeField] Text lbLoading;

    void Awake()
    {
        btnButton.gameObject.SetActive(true);
        lbLoading.gameObject.SetActive(false);
    }

    public void StartGame()
    {   btnButton.gameObject.SetActive(true);
        lbLoading.gameObject.SetActive(false);
        StartCoroutine(LoadScene());
    }

    IEnumerator LoadScene()
    {
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync("s_main");

        while (!asyncLoad.isDone)
        {
            yield return null;
        }
    }
}
