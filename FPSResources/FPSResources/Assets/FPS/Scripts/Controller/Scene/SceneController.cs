﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneController : Singleton<SceneController>
{
    [SerializeField] SceneObject[] m_SceneObjects;
    [SerializeField] string CurrentScene;

    private Dictionary<string, SceneObject> listScenes = new Dictionary<string, SceneObject>();

    private Dictionary<string, Bounds> listBounds = new Dictionary<string, Bounds>();
    private Dictionary<string, SceneData> listDatas = new Dictionary<string, SceneData>();

    public List<string> activeScene = new List<string>();
    List<string> deactiveScene = new List<string>();
    private Queue<string> unloadScene = new Queue<string>();
    private Queue<string> loadScene = new Queue<string>();

    public bool IsInit { get; private set; }  = false;
    bool isReady = false;
    public bool IsAvaiBound => listBounds.ContainsKey(CurrentScene);

    void Awake()
    {
        IsInit = false;
        Init(this);
        CopyData();
        IsInit = true;
    }

    void CopyData()
    {
        if (m_SceneObjects == null || m_SceneObjects.Length == 0) return;
        listScenes.Clear();
        for(int i = 0, n = m_SceneObjects.Length; i < n; i++)
        {
            listScenes.Add(m_SceneObjects[i].m_Identify, m_SceneObjects[i]);
        }
    }

    public void Tick(Vector3 pos)
    {
        if (!isReady) return; 
        if (listBounds.Count < m_SceneObjects.Length) return;

        string ss = getClosestArea(pos);
        if (!ss.Equals(CurrentScene))
        {
            StartCoroutine(LoadNeedScenes(ss, () =>
                    {
                        InGameController.OnEnterNewScene();
                    }));
        }

        //var b = listBounds[CurrentScene];
        //if (isInArea(pos, b)) return;
        //for(int i = 0; i < activeScene.Count; i++)
        //{
        //    if (activeScene[i] != CurrentScene)
        //    {
        //        if (!listBounds.ContainsKey(activeScene[i])) return;
        //        var c = listBounds[activeScene[i]];
        //        if (isInArea(pos, c))
        //        {
        //            LoadScenes(activeScene[i], ()=>
        //            {
        //                InGameController.OnEnterNewScene();
        //            });
        //            return;
        //        }
        //    }
        //}
    }

    string getClosestArea(Vector3 pos)
    {
        string a = CurrentScene;
        float ss = Helper.Distance(pos, listBounds[CurrentScene].center);
        for (int i = 0; i < activeScene.Count; i++)
        {
            if (activeScene[i] != a)
            {
                float s1 = Helper.Distance(pos, listBounds[activeScene[i]].center); 
                if (s1 < ss)
                {
                    ss = s1; 
                    a = activeScene[i];
                }
            }
        }
        return a;
    }

#if TYPE_1
    public void LoadScenes(string _scene, System.Action onFinish = null)
    {
        isReady = false;
        CurrentScene = _scene;
        SceneObject ss = listScenes[_scene];
        List<string> ll = new List<string>();
        ll.Add(ss.m_Identify);
        if (ss.NearlyArea != null) ll.AddRange(ss.NearlyArea);
        StartCoroutine(LoadAllScene(ll.ToArray(), ()=>
        {
            isReady = true;
            if (onFinish != null) onFinish.Invoke();
        }));
    }

    IEnumerator LoadAllScene(string[] list, System.Action onFinish)
    {
        if (list == null || list.Length == 0) yield break;
        unloadScene.Clear();
        loadScene.Clear();
        List<string> keep = new List<string>();
        //Prepare Scene
        for(int i = 0; i < list.Length; i++)
        {
            if (!activeScene.Contains(list[i]))
                loadScene.Enqueue(list[i]);
            else keep.Add(list[i]);
        }

        for(int i = 0; i < activeScene.Count; i++)
        {
            if (!loadScene.Contains(activeScene[i]) && !keep.Contains(activeScene[i]))
                unloadScene.Enqueue(activeScene[i]);
        }

        yield return DoUnloadScene(unloadScene);
        
        yield return DoLoadScene(loadScene);

        if(onFinish != null) onFinish.Invoke();
    }
#else

    static bool isLoaded = false;
    public void LoadScenes(string _scene, System.Action onFinish = null)
    {
        isReady = false;
        if (!isLoaded)
        {
            isLoaded = true;
            List<string> kk = new List<string>();
            for (int i = 0, n = m_SceneObjects.Length; i < n; i++)
            {
                kk.Add(m_SceneObjects[i].m_Identify);
            }

            StartCoroutine(LoadAllScene(kk.ToArray(), () =>
            {
                StartCoroutine(LoadNeedScene(_scene, onFinish));
            }));
        }
        else
        {
            StartCoroutine(LoadNeedScenes(_scene, onFinish));
        }
    }

    IEnumerator LoadNeedScene(string _scene, System.Action onFinish = null)
    {
        
        CurrentScene = _scene;
        SceneObject ss = listScenes[_scene];
        List<string> ll = new List<string>();
        ll.Add(ss.m_Identify);
        if (ss.NearlyArea != null) ll.AddRange(ss.NearlyArea);

        while (listDatas.Count < listScenes.Count) yield return null;

        for (int i = 0; i < ll.Count; i++)
        {
            listDatas[ll[i]].gameObject.SetActive(true);
            activeScene.Add(ll[i]);
        }
        isReady = true;
        if (onFinish != null) onFinish.Invoke();
    }

    IEnumerator LoadNeedScenes(string _scene, System.Action onFinish = null)
    {
        isReady = false;
        CurrentScene = _scene;
        SceneObject ss = listScenes[_scene];
        List<string> list = new List<string>();
        list.Add(ss.m_Identify);
        if (ss.NearlyArea != null) list.AddRange(ss.NearlyArea);

        while (listDatas.Count < listScenes.Count) yield return null;

        if (list == null || list.Count == 0) yield break;
        unloadScene.Clear();
        loadScene.Clear();
        List<string> keep = new List<string>();
        //Prepare Scene
        for (int i = 0; i < list.Count; i++)
        {
            if (!activeScene.Contains(list[i]))
                loadScene.Enqueue(list[i]);
            else keep.Add(list[i]);
        }

        for (int i = 0; i < activeScene.Count; i++)
        {
            if (!loadScene.Contains(activeScene[i]) && !keep.Contains(activeScene[i]))
                unloadScene.Enqueue(activeScene[i]);
        }

        yield return DoShowScene(loadScene);

        yield return DoHideScene(unloadScene);

        isReady = true;
        if (onFinish != null) onFinish.Invoke();
    }

    IEnumerator LoadAllScene(string[] list, System.Action onFinish)
    {
        if (list == null || list.Length == 0) yield break;
        unloadScene.Clear();
        loadScene.Clear();
        List<string> keep = new List<string>();
        //Prepare Scene
        for (int i = 0; i < list.Length; i++)
        {
            loadScene.Enqueue(list[i]);
        }

        yield return DoLoadScene(loadScene);

        if (onFinish != null) onFinish.Invoke();
    }

    IEnumerator DoShowScene(Queue<string> scene)
    {
        while (scene.Count > 0)
        {
            string ss = scene.Dequeue();
            deactiveScene.Remove(ss);
            activeScene.Add(ss);
            listDatas[ss].gameObject.SetActive(true);
            yield return new WaitForEndOfFrame();
        }
    }

    IEnumerator DoHideScene(Queue<string> scene)
    {
        while (scene.Count > 0)
        {
            string ss = scene.Dequeue();
            activeScene.Remove(ss);
            listDatas[ss].gameObject.SetActive(false);
            deactiveScene.Add(ss);
            yield return new WaitForEndOfFrame();
        }
    }

#endif

    IEnumerator DoLoadScene(Queue<string> scene)
    {
        while (scene.Count > 0)
        {
            string ss = scene.Dequeue();
            AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(ss, LoadSceneMode.Additive);
            //activeScene.Add(ss);
            // Wait until the asynchronous scene fully loads
            while (!asyncLoad.isDone)
            {
                yield return null;
            }

            yield return new WaitForSeconds(0.05f);
        }
    }

    IEnumerator DoUnloadScene(Queue<string> scene)
    {
        while (scene.Count > 0)
        {
            string ss = scene.Dequeue();
            //activeScene.Remove(ss);
            AsyncOperation asyncLoad = SceneManager.UnloadSceneAsync(ss);

            // Wait until the asynchronous scene fully loads
            while (!asyncLoad.isDone)
            {
                yield return null;
            }
            //deactiveScene.Add(ss);
            yield return new WaitForEndOfFrame();
        }
    }

    public static void OnLoadSceneFinish(string scene, Bounds bound, SceneData data)
    {
        if (!SceneController.Exists()) return;
        if (SceneController.Instance.listBounds.ContainsKey(scene)) SceneController.Instance.listBounds[scene] = bound;
        else SceneController.Instance.listBounds.Add(scene, bound);

        if (SceneController.Instance.listDatas.ContainsKey(scene)) SceneController.Instance.listDatas[scene] = data;
        else SceneController.Instance.listDatas.Add(scene, data);
    }

    public bool isInArea(Vector3 point, Bounds m_bound)
    {
        return !(point.x < m_bound.min.x || point.x > m_bound.max.x || point.z < m_bound.min.z || point.z > m_bound.max.z);
    }

    public Bounds getCurrentBounds()
    {
        return listBounds[CurrentScene];
    }
}
