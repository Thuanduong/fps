﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneData : MonoBehaviour
{
    [SerializeField] string m_identify;
    [SerializeField] GameObject center;

    void Start()
    {
        if(center != null)
        {
            SceneController.OnLoadSceneFinish(m_identify, GetBounds(), this);
            this.gameObject.SetActive(false);
        }
    }

    Bounds GetBounds()
    {
        Collider[] cols = center.GetComponents<Collider>();
        Bounds _worldBox = new Bounds();
        for (var i = 0; i < cols.Length; ++i)
        {
            _worldBox.Encapsulate(cols[i].bounds);
        }
        return _worldBox;
    }
}
