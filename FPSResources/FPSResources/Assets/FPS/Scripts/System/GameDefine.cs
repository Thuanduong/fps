﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GameDefine
{
    public enum CHARACTER_TYPE
    {
        PLAYER = 0,
        ENEMY = 1 << 0,
        NPC_NORMAL = 1 << 1,
        NPC_FOLLOW = 1 << 2,
    }

    public enum WEAPON_TYPE
    {
        No_Weapon = 0,
        Pistol = 1,
        AssultRifle01 = 2,
        AssultRifle02 = 3,
        Shotgun = 4,
        SniperRifle = 5,
        Rifle = 6,
        SubMachineGun = 7,
        RPG = 8,
        MiniGun = 9,
        Grenades = 10,
        Bow = 11,
        Melee = 12,
    }

    public enum ANIMATION_TYPE
    {
        Normal = 0,
        Crossed_Arms = 1,
        HandsOnHips = 2,
        Check_Watch = 3,
        Sexy_Dance = 4,
        Smoking = 5,
        Salute = 6,
        Wipe_Mount = 7,
        Leaning_against_wall = 8,
        Sitting_on_Ground = 9
    }

    public enum ENEMY_LEVEL
    {
        LVL_1 = 0,
        LVL_2 = 1
    }
}
