﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Helper 
{
    public static float Distance(Vector3 p1, Vector3 p2)
    {
        return Mathf.Sqrt(Mathf.Abs((p1.x - p2.x) * (p1.x - p2.x) + (p1.z - p2.z) * (p1.z - p2.z)));
    }
}
