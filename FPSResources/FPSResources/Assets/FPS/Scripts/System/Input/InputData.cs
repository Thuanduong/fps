﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputData
{

    #region Data
    protected Vector2 m_inputVector;
    #endregion

    public Vector2 InputVector => m_inputVector;
    public bool HasInput => m_inputVector != Vector2.zero;

    public float InputVectorX
    {
        set => m_inputVector.x = value;
    }

    public float InputVectorY
    {
        set => m_inputVector.y = value;
    }

    public virtual void ResetInput()
    {
        m_inputVector = Vector2.zero;
    }
}

public class MovementInputData : InputData
{
    bool m_jumpClicked;
    bool m_shotClicked;

    public bool JumpClicked
    {
        get => m_jumpClicked;
        set => m_jumpClicked = value;
    }

    public bool ShotClicked
    {
        get => m_shotClicked;
        set => m_shotClicked = value;
    }


    public override void ResetInput()
    {
        m_inputVector = Vector2.zero;

        m_jumpClicked = false;
    }
}