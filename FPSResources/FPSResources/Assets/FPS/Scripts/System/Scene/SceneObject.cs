﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SceneObject 
{
    public string m_Identify;
    [SerializeField] string[] m_nearlyArea;
    [SerializeField] string[] m_nearlyObjectActive;
    [SerializeField] float X_Min, X_Max;
    [SerializeField] float Z_Min, Z_Max;

    private Bounds m_bound;
    public SceneObject setBound(Bounds bound)
    {
        this.m_bound = bound;
        return this;
    }

    public bool isInArea(Vector3 point)
    {
        //return !(point.x < X_Min || point.x > X_Max || point.z < Z_Min || point.z > Z_Max);
        return this.m_bound.Contains(point);
    }

    public string[] NearlyArea
    {
        get { return m_nearlyArea; }
    }
}
