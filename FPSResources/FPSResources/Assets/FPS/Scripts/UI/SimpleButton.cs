﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SimpleButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    bool m_press = false;
    bool m_recentPress = false;

    public bool hasPress => m_press;

    public void OnPointerDown(PointerEventData eventData)
    {
        m_press = true;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        m_press = false;
    }
}
