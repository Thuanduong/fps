﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class UIManager : Singleton<UIManager>
{
    [SerializeField]  CanvasGroup blockLayout;
    [SerializeField] Text lbScore;
    [SerializeField] Text lbHP;

    void Awake()
    {
        Init(this);
    }

    public static void ShowBlock(bool value)
    {
        if (!Exists()) return;
        if (value)
        {
            Instance.blockLayout.gameObject.SetActive(true);
            Instance.blockLayout.DOFade(1, 0.5f).ChangeStartValue(0);
        }
        else
        {
            Instance.blockLayout.gameObject.SetActive(true);
            Instance.blockLayout.DOFade(0, 0.5f).ChangeStartValue(1).OnComplete(() =>
            {
                Instance.blockLayout.gameObject.SetActive(false);
            });
        }
    }

    public static void SetScore(int value)
    {
        if (!Exists()) return;
        Instance.lbScore.text = value.ToString();
    }

    public static void SetCurrentHP(int value)
    {
        if (!Exists()) return;
        Instance.lbHP.text = value.ToString();
    }
}
